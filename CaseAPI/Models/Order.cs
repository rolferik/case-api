﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseApi.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public Guid UserId { get; set; }
        public DateTime OrderedDate { get; set; }
        public virtual ICollection<CartItem> ShoppingCartItems { get; set; }
    }
}
