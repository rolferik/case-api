﻿using CaseApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseApi.Models
{
    public class CartItem
    {
        [Key]
        public int ItemId { get; set; }
        public Guid UserId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateCreated { get; set; }
        public Product Product { get; set; }

        #nullable enable
        public Order? Order { get; set; }
        #nullable disable
    }
}
