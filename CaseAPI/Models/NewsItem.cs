﻿using CaseApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseApi.Models
{
    public class NewsItem
    {
        [Key]
        public int NewsId { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime ValidUntil { get; set; }
        public Product Product { get; set; }

    }
}
