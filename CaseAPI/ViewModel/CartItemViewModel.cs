﻿using CaseApi.Models;
using System;
using System.Linq;

namespace CaseAPI.ViewModel
{
    public class CartItemViewModel
    {
        public int ItemId { get; set; }
        public Guid CartId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateCreated { get; set; }
        public ProductViewModel Product { get; set; }

        public CartItemViewModel() { }
        public CartItemViewModel(CartItem model)
        {
            ItemId = model.ItemId;
            CartId = model.UserId;
            Quantity = model.Quantity;
            DateCreated = model.DateCreated;
            Product = new ProductViewModel(model.Product);
        }
    }
}
