﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseAPI.ViewModel
{
    public class SummaryViewModel
    {
        public List<CartItemViewModel> ShoppingCart { get; set; }
        public double Cost
        {
            get
            {
                var cost = 0.0;
                if (ShoppingCart == null)
                    return cost;

                ShoppingCart.ForEach(item => cost += (item.Product.Price * item.Quantity));

                return cost;
            }
        }
    }
}
