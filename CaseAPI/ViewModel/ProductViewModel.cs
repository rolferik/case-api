﻿using CaseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseAPI.ViewModel
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }

        public ProductViewModel() { }

        public ProductViewModel(Product model)
        {
            ProductId = model.ProductId;
            Name = model.Name;
            Description = model.Description;
            Price = model.Price;
        }
    }
}
