﻿using CaseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseAPI.ViewModel
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public Guid UserId { get; set; }
        public ICollection<CartItemViewModel> ShoppingCart { get; set; }

        public OrderViewModel() { }
        public OrderViewModel(Order model)
        {
            OrderId = model.OrderId;
            UserId = model.UserId;
            ShoppingCart = model.ShoppingCartItems.Select(ci => new CartItemViewModel(ci)).ToList();
        }
    }
}
