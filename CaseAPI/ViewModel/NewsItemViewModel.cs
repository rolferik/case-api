﻿using CaseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseAPI.ViewModel
{
    public class NewsItemViewModel
    {
        public int NewsId { get; set; }
        public DateTime Created { get; set; }
        public DateTime ValidUntil { get; set; }
        public ProductViewModel Product { get; set; }

        public NewsItemViewModel(NewsItem model)
        {
            NewsId = model.NewsId;
            Created = model.Created;
            ValidUntil = model.ValidUntil;
            Product = new ProductViewModel(model.Product);
        } 
    }
}
