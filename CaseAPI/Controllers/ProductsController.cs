﻿using CaseApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseAPI.Paging;
using CaseAPI.ViewModel;
using Microsoft.AspNetCore.Authorization;
using CaseAPI.Authentication;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private ApplicationDbContext _db;

        public ProductsController(ApplicationDbContext dbContext)
        {
            _db = dbContext;

            CreateTestData();
        }

        private void CreateTestData()
        {
            for (int i = 0; i < 10; i++)
            {
                if (_db.Products.Count(p => p.ProductId == i) > 0)
                    continue;

                var testProduct = new Product
                {
                    Name = $"Test{i}",
                    Description = $"Test{i} Description",
                    Price = i * 7.33,
                };

                _db.Products.Add(testProduct);
            }

            _db.SaveChanges();
        }

        // GET: api/<ProductsController>
        [HttpGet]
        public ActionResult Get([FromQuery] string search = "", [FromQuery] int page = 0, [FromQuery] int pageSize = 10)
        {
            var products = _db.Products.Where(p => p.Name.Contains(search));
            if (page == 0)
            {
                return Ok(products.Select(p => new ProductViewModel(p)).ToList());
            }

            var pagedResult = products.GetPaged(page, pageSize);

            if (pagedResult.CurrentPage < pagedResult.PageCount)
                return StatusCode(206, pagedResult.Results.Select(p => new ProductViewModel(p)));
            else if (pagedResult.CurrentPage == pagedResult.PageCount)
                return Ok(pagedResult.Results.Select(p => new ProductViewModel(p)).ToList());

            else return NoContent();

        }

        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public ActionResult Post([FromBody] Product product)
        {
            var existing = _db.Products.Find(product.ProductId);

            if (existing != null)
                return BadRequest("Product already exists");
            else
                _db.Products.Add(product);

            _db.SaveChanges();

            return StatusCode(201, new ProductViewModel(product));
        }

        // GET api/<ProductsController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var product = _db.Products.Find(id);

            if (product == null)
                return NoContent();

            return Ok(new ProductViewModel(product));
        }

        // PUT api/<ProductsController>/5
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Product value)
        {
            var product = _db.Products.Find(id);

            if (product == null)
                return NotFound(new ProductViewModel(value));

            product.Name = value?.Name ?? product.Name;
            product.Description = value?.Description ?? product.Description;
            product.Price = value.Price > 0 ? value.Price : product.Price;

            _db.Products.Update(product);
            _db.SaveChanges();

            return Ok(new ProductViewModel(product));
        }

        // DELETE api/<ProductsController>/5
        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var productToRemove = _db.Products.Find(id);

            if (productToRemove == null)
                return NoContent();

            _db.Products.Remove(productToRemove);
            _db.SaveChanges();

            return Ok();
        }
    }
}
