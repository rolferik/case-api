﻿using CaseApi.Models;
using CaseAPI.Authentication;
using CaseAPI.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CaseAPI.Controllers
{
    //Inspired from https://docs.microsoft.com/en-us/aspnet/web-forms/overview/getting-started/getting-started-with-aspnet-45-web-forms/shopping-cart
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private ApplicationDbContext _db;
        public ShoppingCartController(ApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        //api/<ShoppingCartController>
        [Authorize(Roles = UserRoles.Customer)]
        [HttpGet]
        public ActionResult Get()
        {
            var userId = GetUserId();

            var cartItems = _db.ShoppingCartItems
                .Where(sc => sc.UserId.Equals(userId) && sc.Order == null)
                .Include(sc => sc.Product)
                .Select(sc => new CartItemViewModel(sc))
                .ToList();

            if (cartItems == null || cartItems.Count == 0)
                return NoContent();

            return Ok(cartItems);
        }

        //api/<ShoppingCartController>/5
        [Authorize(Roles = UserRoles.Customer)]
        [HttpPost("{id}")]
        public ActionResult AddToCart(int id)
        {
            var userId = GetUserId();

            var cartItem = _db.ShoppingCartItems
                .Where(ci => ci.UserId.Equals(userId) && ci.Product.ProductId == id)
                .Include(ci => ci.Product)
                .FirstOrDefault();

            var product = _db.Products.Find(id);

            if (product == null)
                return BadRequest();

            if (cartItem == null)
            {
                cartItem = new CartItem
                {
                    UserId = userId,
                    DateCreated = DateTime.Now,
                    Product = product,
                    Quantity = 1
                };

                _db.ShoppingCartItems.Add(cartItem);
                _db.SaveChanges();
                return StatusCode(201, new CartItemViewModel(cartItem));
            }
            else
                cartItem.Quantity++;

            _db.SaveChanges();

            return Ok(new CartItemViewModel(cartItem));
        }

        //api/<ShoppingCartController>/5
        [Authorize(Roles = UserRoles.Customer)]
        [HttpDelete("{id}")]
        public ActionResult RemoveFromCart(int id)
        {
            var userId = GetUserId();

            var cartItem = _db.ShoppingCartItems.SingleOrDefault(
                    ci => ci.UserId.Equals(userId) &&
                    ci.Product.ProductId == id);

            if (cartItem == null)
                return NoContent();

            var product = _db.Products.Find(id);

            if (product == null)
                return NoContent();

            if (cartItem.Quantity > 1)
                cartItem.Quantity--;
            else
                _db.ShoppingCartItems.Remove(cartItem);

            _db.SaveChanges();

            return Ok();
        }

        //api/<ShoppingCartController>/summary
        [Authorize(Roles = UserRoles.Customer)]
        [HttpGet("summary")]
        public ActionResult GetSummary()
        {
            var userId = GetUserId();

            var shoppingCart = _db.ShoppingCartItems
                .Where(ci => ci.UserId == userId && ci.Order == null)
                .Include(ci => ci.Product)
                .Select(ci => new CartItemViewModel(ci))
                .ToList();

            if (shoppingCart == null || shoppingCart.Count == 0)
                return NoContent();

            var summary = new SummaryViewModel
            {
                ShoppingCart = shoppingCart,
            };

            return Ok(summary);
        }

        public Guid GetUserId()
        {
            var user = _db.Users.SingleOrDefault(u => u.UserName == User.Identity.Name);
            return Guid.Parse(user.Id);
        }
    }
}
