﻿using CaseApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaseAPI.ViewModel;
using CaseAPI.Authentication;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private ApplicationDbContext _db;

        public OrdersController(ApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        // GET: api/<OrdersController>
        [Authorize(Roles = UserRoles.Customer)]
        [HttpGet]
        public ActionResult Get()
        {
            var userId = GetUserId();

            if (userId == Guid.Empty)
                return BadRequest();

            var orders = _db.Orders
                .Where(o => o.UserId.Equals(userId))
                .Include(o => o.ShoppingCartItems)
                .ThenInclude(o => o.Product)
                .Select(o => new OrderViewModel(o))
                .ToList();

            if (orders == null || orders.Count == 0)
                return NoContent();

            return Ok(orders);
        }

        // GET api/<OrdersController>/5
        [Authorize(Roles = UserRoles.Customer)]
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var user = GetUserId();

            if (user == Guid.Empty)
                return BadRequest();

            var order = _db.Orders
                .Where(o => o.OrderId == id && o.UserId == user)
                .Include(o => o.ShoppingCartItems)
                .ThenInclude(ci => ci.Product)
                .FirstOrDefault();

            if (order != null)
                return Ok(new OrderViewModel(order));

            return NoContent();
        }

        // DELETE api/<OrdersController>/5
        [Authorize(Roles = UserRoles.Customer)]
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var user = GetUserId();

            if (user == Guid.Empty)
                return BadRequest();

            var order = _db.Orders
                .Where(o => o.OrderId == id)
                .Include(o => o.ShoppingCartItems)
                .FirstOrDefault();

            if (order == null)
                return NoContent();

            _db.Orders.Remove(order);
            _db.SaveChanges();
            return Ok();
        }

        // POST api/<OrdersController>/purchase
        [Authorize(Roles = UserRoles.Customer)]
        [HttpPost("purchase")]
        public ActionResult PurchaseShoppingCart()
        {
            var user = GetUserId();

            if (user == Guid.Empty)
                return BadRequest();

            //find all shopping cart item without order ID
            var cartItems = _db.ShoppingCartItems
                .Where(ci => ci.Order == null && ci.UserId.Equals(user))
                .Include(ci => ci.Product)
                .ToList();

            var order = new Order
            {
                ShoppingCartItems = cartItems,
                UserId = user,
                OrderedDate = DateTime.Now
            };

            _db.Orders.Add(order);
            _db.SaveChanges();


            //update CartItems, they are now an order.
            foreach (var ci in cartItems)
            {
                ci.Order = order;
            }

            _db.SaveChanges();

            return StatusCode(201, new OrderViewModel(order));
        }

        private Guid GetUserId()
        {
            var user = _db.Users.SingleOrDefault(u => u.UserName == User.Identity.Name);

            if (user != null)
                return Guid.Parse(user.Id);

            return Guid.Empty;
        }
    }
}
