﻿using CaseApi.Models;
using CaseAPI.Authentication;
using CaseAPI.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private ApplicationDbContext _db;

        public NewsController(ApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        //GET /api/<NewsController>
        [HttpGet]
        public ActionResult Get()
        {
            var news = _db.NewsItems
                .Where(n => n.ValidUntil > DateTime.Now)
                .OrderByDescending(n => n.Created)
                .Include(n => n.Product)
                .Select(n => new NewsItemViewModel(n))
                .ToList();

            if (news == null || news.Count == 0)
                return NoContent();

            return Ok(news);
        }

        //POST /api/<NewsController>
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPost]
        public ActionResult PostNewsItem([FromBody] NewsItem newsItem)
        {
            var existing = _db.NewsItems
                .Where(n => n.NewsId == newsItem.NewsId)
                .FirstOrDefault();

            if (existing != null)
                return BadRequest("Already exists");

            var product = _db.Products.Find(newsItem.Product.ProductId);

            if (product == null)
                return BadRequest();

            newsItem.Product = product;

            _db.NewsItems.Add(newsItem);
            _db.SaveChanges();

            return StatusCode(201, new NewsItemViewModel(newsItem));
        }

        //GET /api/<NewsController>/4
        [HttpGet("{id}")]
        public ActionResult GetNewsItem(int id)
        {
            var newsItem = _db.NewsItems
                .Where(n => n.NewsId == id)
                .Include(n => n.Product)
                .FirstOrDefault();

            if (newsItem == null)
                return NoContent();

            return Ok(new NewsItemViewModel(newsItem));
        }

        //PUT /api/<NewsController>/4
        [Authorize(Roles = UserRoles.Admin)]
        [HttpPut("{id}")]
        public ActionResult PutNewsItem(int id, [FromBody] NewsItem newsItem)
        {
            var existing = _db.NewsItems
                .Where(n => n.NewsId == id)
                .Include(n => n.Product)
                .FirstOrDefault();

            if (existing == null)
                return NoContent();

            var product = _db.Products
                .Where(n => n.ProductId == newsItem.Product.ProductId)
                .FirstOrDefault();

            if (product == null)
                return BadRequest();

            existing.ValidUntil = newsItem?.ValidUntil ?? existing.ValidUntil;
            existing.Product = product;

            _db.SaveChanges();

            return Ok(new NewsItemViewModel(existing));
        }

        //DELETE /api/<NewsController>/4
        [Authorize(Roles = UserRoles.Admin)]
        [HttpDelete("{id}")]
        public ActionResult DeleteNewsItem(int id)
        {
            var existing = _db.NewsItems.Find(id);

            if (existing == null)
                return NoContent();

            _db.NewsItems.Remove(existing);
            _db.SaveChanges();

            return Ok();
        }
    }
}
