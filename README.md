# Deployment

## Requirements

- .NET Core 3.1
- SQL server (integrated in visual studio works

## AppSettings

Change ConnectionString "ConnStr" to whichever SQL server you want

## Entity Framework

### in Visual Studio
1. Open Packadge Management Console
2. Run command: add-migration Initial
3. Run command: update-database

---

# Authenticate API
For this example. it can create users rather easily. If this were to be a permanent solution, it would of course be changed.

THe Api uses Bearer Token to authenticate users.

## /api/authenticate/register

### POST:
Creates a Customer user

*Excample:
{
  "username" : "customer1",
  "Email" : "customer@test.com",
  "password" : "Password1"
}*

**Returns:**

- BadREquest if user exists or some info is missing
- Ok with success message

## /api/authenticate/register-admin

### POST:

Works the same as normal register, except this creates admin users.

*Excample:
{
  "username" : "admin1",
  "Email" : "admin@test.com",
  "password" : "Password12"
}*

**Returns:**

- BadREquest if user exists or some info is missing
- Ok with success message

## /api/authenticate/login

### POST:

Logs the user in, and retrieves a bearer token with an expiry datetime.


*Example:
{
  "username" : "admin1",
  "password" : "Password12"
}*

---

# Products API

## /api/products

### GET:

Retrieves all products from database. Can be filtered.

**Filters:**

- Search: /api/products?search=
- Pagination: /api/products?page=1&pagesize=5 (default pagesize is 10)

**Returns:**

- NoContent if nothing is found.
- Ok(Products) if it finds anything

### POST:
Saves product to database. Requires json in body.

**Authorization:**
Admin only

*Example:
{
	"Name" : "Product1",
	"Description" : "Product1 description"
}*

**Returns:**

- BadRequest if the product already exists.
- Status code 201 if successfull, with the saved product.

## /api/products/id
### GET: 

Retrieves Product with specified id

**Returns:**

- NoContent if nothing is found
- Ok(content) if found.

### PUT:

Updates the existing product using Json from body. Will not update fields that are not specified in Json.

**Authorization:**
Admin only

*Example:
{
	"ProductId" : 1,
	"Name" : "ProductUpdated1"
}*

**Returns:**

- NotFound if it doesn't exist.
- Ok(content) if it does.

### DELETE:

Deletes the specified product.

**Returns:**

- NoContent if nothing exists with the Id.
- Ok if it managed to delete.

---

# ShoppingCart API

## /api/shoppingcart

### GET:

Returns the shopping cart. 

**Authorization:**
Customer only

**Returns:**

- Ok CarItemsViewModel list.

## /api/shoppingcart/id

Id represents the Product Id you want to add to cart.

### POST:

Adds a new CartItem row, with product based on Id.
Will update quantity if CartItem with product id already exists.

**Authorization:**
Customer only

**Returns:**

- BadRequest if the product is not found
- 201 if the CartItem did not exist
- Ok if it managed to update.

### DELETE:

Will remove product a quantity of the Product from CartItem. If no more products are available, it will remove CartItem from database.

**Returns:**

- NoContent if no CartItem is found.
- NoContent if no valid product is found based on the Id
- Ok if successful.

## /api/shoppingcart/summary

### GET:

Will retrieve a summary of the ShoppingCart.

**Returns:**

- NoContent if no CartItems were found.
- Ok with summary

---

# Orders API

## /api/orders

### GET:

Retrieves all orders for the customer

**Authorization:**
Customer only

**Returns:**

- NoContent if nothing is found
- Ok with content if found

## /api/orders/id

### GET:

Retrieves order with specified Id.

**Authorization:**
Customer only

**Returns:**

- NoContent if nothing is found
- Ok if found.

### DELETE:

Deletes order with specified Id.

**Authorization:**
Customer only

**Returns:**

- NoContent if nothing was found
- Ok if it managed to delete

## /api/orders/purchase

### POST:
- Finds all CartItems the user has created, that are not in a valid order (ShoppingCart)
- Creates new Order and references the CartItems
- Updates CartItems with correct OrderId

**Authorization:**
Customer only

---

# News API

## /api/news

### GET:

Retrieves all News Items that are valid.

**Returns:**

- NoContent if no valid items are found
- Ok with content list

### POST:

Creates a new NewsItem with reference to a product.

**Authorization:**
Admin only

**Returns:**

- BadRequest if the product was invalid
- Ok with item created

## /api/news/id

### GET:

Retrieves news item with specified Id

**Returns**
- NoContent if nothing is found
- Ok with content.

### PUT:

Updates the NewsItem with json from Body.

**Authorization:**
Admin only

*Example:
{
    "ValidUntil" : "2021-03-30",
    "Product" : {"ProductId" : 5}
}*

**Returns:**

- NoContent if the NewsItem does not exist
- BadRequest if the product id is not valid
- Ok if it saved.

### DELETE:

Deletes the specified news object

**Authorization:**
Admin only

**Returns:**

- NoContent if no data was found to match the Id
- Ok if successful